// const http = require('http');
// const fs = require('fs');
// const url = require('url');
//
//
// http.createServer((req, res) => {
//     const {
//         pathname,
//         query
//     } = url.parse(req.url, true);
//
//     if (pathname === '/') {
//         res.writeHead(200, {
//             'Content-Type': 'text/html'
//         });
//         res.end('<h1>Hello world</h1>', 'utf-8');
//     } else if (pathname === '/form') {
//         fs.readFile('./create.html', 'utf-8', (err, data) => {
//             console.log(err)
//             if (!err) {
//                 res.writeHead(200, {
//                     'Content-Type': 'text/html'
//                 });
//                 res.end(data, 'utf-8');
//             } else {
//                 res.writeHead(500, {
//                     'Content-Type': 'text/html'
//                 });
//                 res.end('<h1>Error</h1>', 'utf-8');
//             }
//         })
//
//     } else if (pathname === '/create') {
//         const content = JSON.stringify(query);
//
//         fs.appendFile('./chat.json', `${content},`, 'utf-8', (err) => {
//             console.log('File writen', err);
//         });
//         res.writeHead(200, {
//             'Content-Type': 'text/html'
//         });
//         res.end('Sended', 'utf-8');
//     } else if (pathname === '/chat') {
//         fs.readFile('./chat.json', 'utf-8', (err, data) => {
//             if (!err) {
//                 const parsedData = JSON.parse(`[${data.slice(0,-1)}]`);
//                 let template = '';
//
//                 for (let message of parsedData) {
//                     template += `
//                         <div>
//                             <h2>From: ${message.name}</h2>
//                             <p>message: ${message.message}</p>
//                         </div>
//                     `;
//                 }
//
//                 res.writeHead(200, {
//                     'Content-Type': 'text/html'
//                 });
//                 res.end(template, 'utf-8');
//             }
//         })
//     } else {
//         res.writeHead(404, {
//             'Content-Type': 'text/html'
//         });
//         res.end('<h1>Not found, go <a href="/">home</a></h1>', 'utf-8');
//     }
//
//
//
// }).listen(8000);
// console.log('Server runned at 127.0.0.1:8000')

const express = require('express');
const port = 8000;
const app = express();
const path = require('path');

// app.set('view engine', 'pug')

app.get('/', (req, res) => {
    //res.sendFile(path.join(__dirname + '/views/index.html'));
    res.render('index')
});

app.get('/about', (req, res) => {
    res.send('About');
});

app.listen(port, (err) => {
    if (!err) {
        console.log(`Server listen at http://127.0.0.1:${port}`);
    } else {
        console.log(err)
    }
})