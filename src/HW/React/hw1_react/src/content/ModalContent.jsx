export const firstModalContent = {
    header: 'Are you sure?',
    text: 'Really? Think about it again!!!'
};

export const secondModalContent = {

    header: 'Do you want to delet this file?',
    text: 'Once you delete this file, it won’t be possible to undo this action. \n' +
        'Are you sure you want to delete it?'
};