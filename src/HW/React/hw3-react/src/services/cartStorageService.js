import {CART_STORAGE_KEY} from "./constants";

export const cartStorageService = {
    getValue:  function () {
        return JSON.parse(localStorage.getItem(CART_STORAGE_KEY)) || [];
    },
    add: function (id) {
        const newValue = [...this.getValue(), id];
        localStorage.setItem(CART_STORAGE_KEY, JSON.stringify(newValue));
        return newValue;
    },
    contains: function (id) {
        return this.getValue().includes(id);
    },
    remove: function (id) {
        const newValue = this.getValue().filter(el => el !== id);
        localStorage.setItem(CART_STORAGE_KEY, JSON.stringify(newValue));
        return newValue;
    },
};
