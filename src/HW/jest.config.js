module.exports = {
    testEnvironment: 'node',
    moduleFIleExtensions: ["js"],
    testMatch: ["**/?(*.)+(spec|test).[jt]s?(x)"]
}
