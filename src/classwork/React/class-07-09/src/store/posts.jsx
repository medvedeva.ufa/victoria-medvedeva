import Axios from "axios";
import { DOMAIN } from '../API'
import { ROUTES } from "../navigation/routes";

const SET_POSTS = 'SET_POSTS';
const ADD_POST = 'ADD_POST';

export const MODULE_NAME = 'posts';
export const selectPostsList = state => state[MODULE_NAME].list;

const initialState = {
    list: []
};

export function reducer(state = initialState, { type, payload }) {
    switch (type) {
        case SET_POSTS:
            return {
                ...state,
                list: payload
            }
        case ADD_POST:
            return {
                ...state,
                list: [...state.list, payload]
            }
        default:
            return state
    }
};


export const setPosts = payload => ({
    type: SET_POSTS,
    payload
});
export const addPost = payload => ({
    type: ADD_POST,
    payload
});


export const getPosts = () => async dispatch => {
    try {
        const { status, data } = await Axios.get(`${DOMAIN}/posts.json`);
        if (status === 200) {
            const postsInArray = Object.keys(data).map(key => ({
                id: key,
                ...data[key]
            })).reverse();

            dispatch(setPosts(postsInArray));
        }

    } catch (error) {

    }
}

export const createPost = (post, history) => async dispatch => {
    try {
        const { status } = await Axios.post(`${DOMAIN}/posts.json`, post);

        if (status === 200) {
            history.push(ROUTES.HOMEPAGE);
            // dispatch(addPost({
            //     id: data.name,
            //     ...post
            // }));
        }
    } catch (error) {

    }
}
