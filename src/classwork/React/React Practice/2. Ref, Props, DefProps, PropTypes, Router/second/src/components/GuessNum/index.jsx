import React, { useState } from 'react';
import { StartScreen } from './StartScreen';
import { GameScreen } from './GameScreen';
import { FinishScreen } from './FinishScreen';
// Создаем игру "Угадай число"
//  На старте пользователь видит кнопку "Начать"
//  При нажатии на эту кнопку она пропадает и на экране возникает поле 
//  ввода и кнопка "Угадать"
//  В поле ввода можно ввести число от 1 до 100
//  При нажатии кнопки есть два варианта:
// -- Вы угадали число - переход на "Экран победы" на котором текст 
// поздравления и кнопка "Сыграть еще"
// -- Вы не угадали - Под кнопкой появляется текст "Загаданное число больше/меньше" 
// в зависимости от задуманного числа

export const GuessGame = () => {
    const [stage, setStage] = useState(1);

    const startGame = () => setStage(2);
    const finishGame = () => setStage(3);

    return (
        <div>
            {stage === 1 && <StartScreen startGame={startGame} />}
            {stage === 2 && <GameScreen finishGame={finishGame} />}
            {stage === 3 && <FinishScreen startGame={startGame} />}
        </div>
    )
}