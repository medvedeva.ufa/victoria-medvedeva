import React from 'react';

import './scss/style.scss';
import {Homepage} from "./components/Homepage";

function App() {
    return (

        <Homepage/>
    );
}

export default App;
