$('.slider-main').slick({
    asNavFor: '.slider-min',
    arrows: false
});


$('.slider-min').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-main',
    arrows: true,
    centerMode: true,
    centerPadding: '0',
    focusOnSelect: true
});





$(function () {
    const TAB_NAMES = {
        ALL: "ALL",
        GRAPHIC_DESIGN: "GRAPHIC_DESIGN",
        WEB_DESIGN: "WEB_DESIGN",
        LANDING_PAGES: "LANDING_PAGES",
        WORDPRESS: "WORDPRESS",
    };
    let currentTab = TAB_NAMES.ALL;
    let itemsToShow = 12;

    function showSelected(selectedValue) {
        let showed = 0;
        let showLoadMoreButton = false;
        $(".portfolio__item").each(function () {
            if (showed >= itemsToShow) {
                $(this).hide();
                showLoadMoreButton = true;
                return;
            }

            const currentValue = $(this).attr("data-value");

            if (selectedValue === TAB_NAMES.ALL || selectedValue === currentValue) {
                showed++;
                $(this).show();
            } else {
                $(this).hide();
            }
        });

        if(showLoadMoreButton) {
            $("#load-more").show();
        } else {
            $("#load-more").hide();
        }
    }

    $("#load-more").click(() => {
        itemsToShow = itemsToShow + 12;
        showSelected(currentTab);
    });

    $("#tabs li").click((event) => {
        let clickedValue = $(event.target).attr("data-value");

        $("#tabs li").each(function () {
            $(this).removeClass("active");
        });

        $(event.target).addClass("active");

        currentTab = clickedValue;
        showSelected(currentTab);
    });

    showSelected(currentTab);

});