import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';


const Heanding = () => {
    const [title, setTitle] = useState('Hello');

    // setTimeout( () => {
    //   setTitle('New title')
    // }, 3000);

    //хук для управления жизненными циклами. Принимает 2 аргумена - функция, которая выполняется, и массив аргументов,
    // за которыми "следят". Если они меняются - фукция запускается повторно, если передать пустой массик (следить не за чем).
    //   useEffect(() => {
    //       setTimeout(() => {
    //           setTitle('New title')
    //       }, 3000);
    //   }, [title]);

    useEffect(() => {
        return () => {
            console.log('unmount')
        }
    });

    return (
        <h1>{title}</h1>
    )

};

function App() {


    // const [status, setStatus] = useState(true );
    // const [counter, setCounter] = useState(0);
    // const btnClicker = () => setStatus(!status);
    // const increaseCounter = () => setCounter(prevCounter => prevCounter + 1);

    const [state, setState] = useState([1, 2, 3, 4, 5, 6, 7]);

    const listOfLi = state.map(num => <li key={num}>{num}</li>);
    // const appendToList = setState(state => [...state, Math.random()]
    // );

    return (
        <div className="App">
            {/*<Counter step='10'/>*/}
            {/*  <ul>*/}
            {/*      {listOfLi}*/}
            {/*  </ul>*/}
            {/*<button>Add</button>*/}
            {/*<TicTacToe/>*/}
            <ColorChanger />

        </div>
    );
}

// const Counter = ({step = 1}) => {
//   const [value, setValue] = useState(0);
//   const minusCounter = () => setValue (v => v - +step);
//   const plusCounter = () => setValue (v => v + +step);
//
//   return (
//       <div>
//         <p>{value}</p>
//         <button onClick={minusCounter}>Minus</button>
//         <button onClick={plusCounter}>Plus</button>
//       </div>
//
//   )
// };

// const TicTacToe = () => {
//   const [grid, setGrid] = useState(Array.from(new Array(9), () => null ));
//   const [nextState, setNextState] = useState('X');
//
//   const makeMove = (cellIndex) => {
//     //console.log(cellIndex);
//     if(grid[cellIndex] === null) {
//       setGrid(prevGrid => prevGrid.map((cell, index) => {
//         if(index === cellIndex) {
//           return nextState;
//         }
//         return cell;
//       }));
//       setNextState(prev => prev === 'X' ? 'O' : 'X');
//     }
//   };
//
//     return (
//         <div className='grid'>
//           {grid.map((cell, i) => (<div key={i} className="cell" onClick={() => makeMove(i)}>
//             <div className="cell-wrap">
//               <span className='cell-symbol'>{cell}</span>
//             </div>
//
//           </div>))}
//
//         </div>
//     )
// };

export const ColorChanger = () => {
  const [list, setList] = useState();
  const addColor = () => {
    const color = randomColor();
  }

  return(
      <div>
        <ul>
          {list.map(item => (<li key={item} style={{color: item, fontSize: 20, padding: 20, border: '1px solid black'}}>{item}</li>))}
        </ul>
        <button>Add color</button>
      </div>

  )
};

export default App;
