const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];


let div = document.querySelector('#root');
let ul = document.createElement('ul');
div.append(ul);
let li = document.createElement('li');

for (let prop of books) {
    if (prop.hasOwnProperty('author') && prop.hasOwnProperty('name') && prop.hasOwnProperty('price')) {
        const {author, name, price} = prop;

        ul.insertAdjacentHTML('beforeend', `<li>Автор: ${author}, Название книги: "${name}", Цена: ${price}</li>`);
    }
    try {
        if (!prop.hasOwnProperty('author')) {
            throw new BooksException(`Property 'author' is not defined.`);
        }
        if (!prop.hasOwnProperty('name')) {
            throw new BooksException(`Property 'name' is not defined.`);
        }
        if (!prop.hasOwnProperty('price')) {
            throw new BooksException(`Property 'price' is not defined.`);
        }
    } catch (error) {
        console.error(`${error.name} : ${error.message}`);
    }
}

function BooksException(message) {
    this.name = 'ReferenceError';
    this.message = message;
}

