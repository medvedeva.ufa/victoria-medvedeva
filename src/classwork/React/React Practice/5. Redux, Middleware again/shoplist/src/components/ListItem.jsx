import React from 'react';
import styled from 'styled-components';

export const ListItem = ({ title, onDeletePress }) => {
    return (
        <Container>
            <Title>{title}</Title>
            <DeleteBtn onClick={onDeletePress}>delete</DeleteBtn>
        </Container>
    )
}

const Container = styled.li`
    display: flex;
    justify-content: space-between;
`

const Title = styled.span`
    font-size: 14px;
`

const DeleteBtn = styled.button`
    background: transparent;
    border: none;
    color: tomato;
`