import React, { useState } from 'react';

export const Counter = ({ step = 1 }) => {
    const [value, setValue] = useState(0);

    const increase = () => setValue(v => v + +step);
    const decrease = () => setValue(v => v - +step);

    return (
        <div>
            <h2>{value}</h2>
            <button onClick={decrease}>-</button>
            <button onClick={increase}>+</button>
        </div>
    )
};