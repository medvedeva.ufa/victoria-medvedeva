const createVisitButton = document.querySelector('.btn-list__item--create');
const enterButton = document.querySelector('.btn-list__item--sign-in');
const createVisitWindow = document.querySelector('.modal');
const enterWindow = document.querySelector('.enter');
const closeBtn = document.querySelector('.close');
const closeIcon = document.querySelector('.enter-window__close-icon');

createVisitButton.addEventListener('click', () => {createVisitWindow.style.display = 'block';});
closeBtn.addEventListener('click', () => {createVisitWindow.style.display = 'none'});

enterButton.addEventListener('click', () => {enterWindow.style.display = 'block';});
closeIcon.addEventListener('click', () => {enterWindow.style.display = 'none'});

