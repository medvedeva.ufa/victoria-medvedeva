import React, {useEffect, useState} from 'react';
import './scss/style.scss'

import {Navigation} from "./navigation";
import {DataContext} from "./DataContext";

function App() {
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('./data.json')
            .then(response => response.json())
            .then(data => setData(data));
    }, []);

    return (
        <DataContext.Provider value={data}>
            <Navigation />
        </DataContext.Provider>
    )
}

export default App;
