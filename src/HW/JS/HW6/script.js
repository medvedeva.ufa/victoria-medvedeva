let array = ['hello', 'world', 23, '23', null];


function filterBy(array, type) {
    let newArray = array.filter(function(number) {
        return typeof number !== type;
    });
    return newArray;
}


console.log(filterBy(array, 'string'));