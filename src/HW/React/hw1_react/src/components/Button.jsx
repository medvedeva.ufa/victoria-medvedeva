import React from "react";

export const Button = ({backgroundColor, text, showFunction, className}) => {
const styles = {
    backgroundColor: backgroundColor,
};
    return (
        <button className={className} style={styles} onClick={showFunction}>{text}</button>
    )
};