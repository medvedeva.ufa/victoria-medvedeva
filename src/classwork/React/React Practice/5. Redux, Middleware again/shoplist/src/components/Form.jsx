import React from 'react';
import styled from 'styled-components';
import { useState } from 'react';
import { connect } from 'react-redux';
import { createItem } from '../store/shopList';



export const Form = connect(null, { createItem })(({ createItem }) => {
    const [value, setValue] = useState('')

    const onSubmit = (e) => {
        e.preventDefault();

        if (value.trim() !== '') {
            createItem(value);
            setValue('');
        }
    }

    return (
        <form onSubmit={onSubmit}>
            <Field name="item" value={value} onChange={e => setValue(e.target.value)} />
        </form>
    )
})

const Field = styled.input`
    background: #f2f2f2;
    border: none;
    border-radius: 40px;
    padding: 15px;
    display: block;
    width: 100%;
    box-sizing: border-box;
    margin-top: 15px;
`