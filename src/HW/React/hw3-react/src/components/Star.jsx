import React from "react";
import PropTypes from 'prop-types';


export const Star = ({className, onClick}) => {
    return (
        <i onClick={onClick} className={className}></i>
    )
};

Star.propTypes = {
    className: PropTypes.string
};

