import React from 'react';

export const FinishScreen = ({ startGame }) => {
    return (
        <div>
            <h1>FinishScreen</h1>
            <button onClick={startGame}>Play again</button>
        </div>
    )
};
