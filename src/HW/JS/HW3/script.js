const firstNumber = +prompt("Enter first number");
const secondNumber = +prompt("Enter second number");
const operation = prompt("Enter operation");

function mathOperation(firstNumber, secondNumber, operation) {
    switch (operation) {
        case "+" : return firstNumber + secondNumber;
        case "-" : return firstNumber - secondNumber;
        case "*" : return firstNumber * secondNumber;
        case "/" : return firstNumber / secondNumber;
    }
}

console.log(mathOperation(firstNumber, secondNumber, operation));
