import axios from "axios";

const SET_ITEMS = 'SET_ITEMS';
const ADD_ITEM = 'ADD_ITEM';
const DELETE_ITEM = 'DELETE_ITEM';

export const MODULE_NAME = 'shoplist';
export const selectShopList = state => state[MODULE_NAME].list;

const initialState = {
    list: []
}

export function reducer(state = initialState, { type, payload }) {
    switch (type) {
        case SET_ITEMS:
            return {
                ...state,
                list: payload
            }
        case ADD_ITEM:
            return {
                ...state,
                list: [...state.list, payload]
            }
        case DELETE_ITEM:
            return {
                ...state,
                list: state.list.filter(({ id }) => id !== payload)
            }
        default:
            return state
    }
}

export const setItems = payload => ({
    type: SET_ITEMS,
    payload
});
export const addItem = payload => ({
    type: ADD_ITEM,
    payload
});
export const deleteItemLocally = payload => ({
    type: DELETE_ITEM,
    payload
});

export const getList = () => async dispatch => {
    try {
        const { data } = await axios.get("http://localhost:3005/list")
        dispatch(setItems(data))

    } catch (error) {
        console.log(error);
    }
};

export const createItem = (text) => async dispatch => {
    try {
        const { status, data } = await axios.post("http://localhost:3005/list", { text });

        if (status === 201) {
            dispatch(addItem(data));
        }

    } catch (error) {
        console.log(error);
    }
};

export const removeItem = (id) => async dispatch => {
    try {
        const { status } = await axios.delete(`http://localhost:3005/list/${id}`);

        if (status === 200) {
            dispatch(deleteItemLocally(id));
        }

    } catch (error) {
        console.log(error);
    }
};