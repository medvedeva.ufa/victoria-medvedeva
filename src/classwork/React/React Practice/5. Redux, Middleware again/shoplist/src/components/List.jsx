import React from 'react';
import { ListItem } from './ListItem'
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import { selectShopList, removeItem } from '../store/shopList';


// connect
// const mapStateToProps = state => ({list: selectShopList(state)})
// connect(mapStateToProps)(...)

export const List = () => {
    const list = useSelector(selectShopList);
    const dispatch = useDispatch();

    const deleteHandler = (id) => {
        removeItem(id)(dispatch);
    }

    return (
        <ListWrapper>
            {list.map(({ id, text }) => (
                <ListItem key={id} title={text} onDeletePress={() => deleteHandler(id)} />
            ))}
        </ListWrapper>
    )
}

const ListWrapper = styled.ul`
    list-style: none;
    padding: 0;
    margin: 15px 0 0;
`