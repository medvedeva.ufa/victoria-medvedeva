import React from "react";
import PropTypes from 'prop-types';
import styled from "styled-components";

export const Button = ({ text, onClick, style}) => {
    return (
        <Btn onClick={onClick} style={style}>{text}</Btn>
    )
};

const Btn = styled.button`
  padding: 5px 10px 5px 10px;
  color: black;
  border: 1px solid transparent;
  border-radius: 10px;
  cursor: pointer;
  background-repeat: no-repeat;
  background-size: 19px;
  background-position: left;
  margin-top: 10px;
`;

Button.propTypes = {
    text: PropTypes.string,
    style: PropTypes.object,
};

Button.defaultProps = {
    text: 'Ok',
    style: {
        backgroundColor: 'white'
    }
};