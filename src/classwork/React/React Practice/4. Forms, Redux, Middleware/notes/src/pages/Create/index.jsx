import React, { useState } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { createNote } from '../../store/notes';

// Создать форму состоящую из таких компонентов
// - Поле title
// - Поле description
// - Поле date
// - Кнопка submit
//  При нажатии на submit или enter форма должна "сабмитится"*
//  -- Если есть незаполненные поля, должен появится алерт с ошибкой
//  -- Если все поля заполнены, в консоли должен появится объект со всеми данными из формы
// * Форма должна сабмитится без перезагрузки страницы

const formInitialState = {
    title: '',
    desc: '',
    date: '',
};

export const Create = connect(null, { createNote })(({ createNote }) => {
    const [fields, setFields] = useState(formInitialState);

    const onChangeHandler = (e) => {
        const { name, value } = e.target;
        setFields(fields => ({
            ...fields,
            [name]: value
        }));
    };

    const onSubmit = e => {
        e.preventDefault();

        for (let key in fields) {
            if (fields[key].trim() === '') {
                alert('Fill all fields');
                return;
            }
        }

        setFields(formInitialState);
        createNote(fields);
    }

    return (
        <Container>
            <Form onSubmit={onSubmit}>
                <Heading>Create</Heading>

                <input value={fields.title} name="title" onChange={onChangeHandler} />
                <textarea value={fields.desc} name="desc" onChange={onChangeHandler} />
                <input value={fields.date} type="date" name="date" onChange={onChangeHandler} />

                <button type="submit">create</button>

            </Form>
        </Container>
    )
})

const Container = styled.section`
    max-width: 1200px;
    padding: 35px 15px;
    margin: 0 auto;
`

const Form = styled.form`
    max-width: 400px;
    margin: 0 auto;

    input, textarea, button {
        box-sizing: border-box;
        display: block;
        width: 100%;
        resize: none;
        &:not(:first-child) {
            margin-top: 10px;
        }
    }
`

const Heading = styled.h2`
    text-align: center;
`