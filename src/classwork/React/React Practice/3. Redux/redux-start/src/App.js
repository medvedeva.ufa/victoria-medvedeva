import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux'

// import logo from './logo.svg';
import './App.css';
import { Homepage } from './components/Homepage';
import { About } from './components/About';
import { Counter } from './components/Counter';

import store from './store';
import { CounterIncrement } from './components/CounterIncrement';


function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/counter">
            <Counter />
            <CounterIncrement />
          </Route>
          <Route path="/about" render={() => (
            <About data="test" />
          )} />
          <Route path="/">
            <Homepage />
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
