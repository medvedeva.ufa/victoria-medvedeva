import React from 'react';
import PropTypes from 'prop-types';

export const PropsTest = ({ name, age, isApproved }) => {
    return (
        <div style={{ border: isApproved ? '1px solid green' : '' }}>
            <h3>{name}</h3>
            <p>{age}</p>
        </div>
    )
};

PropsTest.propTypes = {
    name: PropTypes.string,
    age: PropTypes.number,
    isApproved: PropTypes.bool
};

PropsTest.defaultProps = {
    name: 'Anonymous',
    age: 0,
    isApproved: false
};