import React, {useState} from 'react';


import {DataContext} from "../../DataContext";
import {CardsList} from "../../components/CardsList";


export const FavoritesPage = ({}) => {
    const filterOnlyInFavorites = (e, favorites) => favorites.includes(e.id);

    return (
        <div>
            <DataContext.Consumer>
                {data =>
                    <CardsList data={data}
                               filterData={filterOnlyInFavorites}
                    />
                }
            </DataContext.Consumer>
        </div>
    );
}