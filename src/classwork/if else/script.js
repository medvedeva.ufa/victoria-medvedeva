
/*
 *
 * Напишите программу «Кофейная машина».
 *
 * Программа принимает монеты и готовит напитки:
 * - Кофе за 25 монет;
 * - Капучино за 50 монет;
 * - Чай за 10 монет.
 *
 * Чтобы программа знала, что делать, она должна знать:
 * - Сколько монет пользователь внёс;
 * - Какой он желает напиток.
 *
 * В зависимости от того, какой напиток выбрал пользователь,
 * программа должна вычислить сдачу и вывести сообщение в консоль:
 * «Ваш «НАЗВАНИЕ НАПИТКА» готов. Возьмите сдачу: «СУММА СДАЧИ».".

* Если пользователь ввёл недостаточную сумму — вывести сообщение:
 * «К сожаению, этой суммы недостаточно, чтобы приобрести чай «НАЗВАНИЕ НАПИТКА». Вам нужно добавить «НЕДОСТАЮЩАЯ СУММА»"

 *
 * Если пользователь ввёл сумму без сдачи — вывести сообщение:
 * «Ваш «НАЗВАНИЕ НАПИТКА» готов. Спасибо за сумму без сдачи! :)"
*/

let chooseDrink = prompt("Choose you drink");
let payForDrink = +prompt("Pay for your drink");
const coffeeCoast = 25;
const teaCoast = 10;
const cappuccinoCoast = 50;


switch (chooseDrink) {
    case 'coffee':
        if (payForDrink < coffeeCoast) {
            alert("К сожалению, этой суммы недостаточно, чтобы приобрести " + chooseDrink + ". Вам нужно добавить "
                + (coffeeCoast - payForDrink) + " долларов");
        } else if (payForDrink === coffeeCoast) {
            alert("Ваш " + chooseDrink + " готов! Спасибо за сумму без сдачи! :)");
        } else {
            alert("Ваш " + chooseDrink + " готов! Возьмите сдачу: " + (payForDrink - coffeeCoast));
        }
        break;
    case 'tea' :
        if (payForDrink < teaCoast) {
            alert("К сожалению, этой суммы недостаточно, чтобы приобрести " + chooseDrink + ". Вам нужно добавить "
                + (teaCoast - payForDrink) + " долларов");
        } else if (payForDrink === teaCoast) {
            alert("Ваш " + chooseDrink + " готов! Спасибо за сумму без сдачи! :)");
        } else {
            alert("Ваш " + chooseDrink + " готов! Возьмите сдачу: " + (payForDrink - teaCoast));
        }
        break;
    case 'cappuccino' :
        if (payForDrink < cappuccinoCoast) {
            alert("К сожалению, этой суммы недостаточно, чтобы приобрести " + chooseDrink + ". Вам нужно добавить "
                + (cappuccinoCoast - payForDrink) + " долларов");
        } else if (payForDrink === cappuccinoCoast) {
            alert("Ваш " + chooseDrink + " готов! Спасибо за сумму без сдачи! :)");
        } else {
            alert("Ваш " + chooseDrink + " готов! Возьмите сдачу: " + (payForDrink - cappuccinoCoast));
        }
        break;
}





