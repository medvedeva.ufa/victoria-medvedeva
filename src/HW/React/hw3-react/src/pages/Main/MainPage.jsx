import React from "react";
import {CardsList} from "../../components/CardsList";
import {DataContext} from "../../DataContext";



export const MainPage = () => {
    return (
        <>
            <DataContext.Consumer>
                {data =>
                    <CardsList data={data} />
                }
            </DataContext.Consumer>
        </>
    )
}