
const iconFirst = document.querySelector("#i-first");
const iconSecond = document.querySelector("#i-second");
const inputFirst = document.querySelector("#first-password");
const inputSecond = document.querySelector("#second-password");
const button = document.querySelector('.btn');


iconFirst.addEventListener("click", () => showPassword(inputFirst, iconFirst));
iconSecond.addEventListener("click", () => showPassword(inputSecond, iconSecond));
button.addEventListener("click", welcome);

function showPassword (element, icon) {
    if(element.getAttribute('type') === 'password') {
        element.removeAttribute('type');
        element.setAttribute('type', 'text');
        icon.className = 'far fa-eye-slash';
    } else {
        element.removeAttribute('type');
        element.setAttribute('type', 'password');
        icon.className = 'far fa-eye';
    }
}

function welcome() {
    if(inputFirst.value === inputSecond.value) {
        alert("you are welcome");
    } else {
        alert("Ваши пароли не совпадают");
    }
}



