import React from 'react';
import { connect } from 'react-redux';

import { addPlanetsAndUpdateNext, setPlanetsTotal } from '../store/planets'
import { useEffect } from 'react';

const mapStateToProps = state => ({
    list: state.planets.list,
    total: state.planets.total,
    nextUrl: state.planets.nextUrl,
});

export const About = connect(mapStateToProps, { addPlanetsAndUpdateNext, setPlanetsTotal })(({
    list, total, nextUrl, addPlanetsAndUpdateNext, setPlanetsTotal
}) => {
    const loadMore = async (isFirstLoad = false) => {
        try {
            const res = await fetch(nextUrl);
            const data = await res.json();

            addPlanetsAndUpdateNext({
                list: data.results,
                url: data.next
            });

            if (isFirstLoad) setPlanetsTotal(data.count);
        } catch (error) {

        }
    }

    useEffect(() => {
        loadMore(true);
    }, [])

    return (
        <div>
            <ul>
                {list.map(({ name, url }) => <li key={url}>{name}</li>)}
            </ul>
            {total > list.length && <button onClick={loadMore}>Load More</button>}
        </div>
    )
})