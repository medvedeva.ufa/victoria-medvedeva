import React, {useState, useEffect} from "react";
import {Card} from "./Card";


export const Container = () => {

    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('./data.json')
            .then(response => response.json())
            .then(data => {
                setData(data)
            });

    }, []);

    return (
        <div className={'container'}>
            <ul className={"cards-list"}>
                {
                    data.map(item =>
                        <Card
                            id={item.id}
                            price={item.price}
                            name={item.name}
                            brand={item.brand}
                            photo={item.photo}
                            color={item.color}
                            article={item.article}
                            key={item.id}
                        />
                    )
                }
            </ul>
        </div>

    )
};