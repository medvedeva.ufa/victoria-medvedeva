import React, { useState, useRef } from 'react';

export const RefTest = () => {
    // const [status, setStatus] = 
    const time = useRef(0);
    // const time2 = +(new Date());

    setInterval(() => {
        time.current = time.current + 1;
    }, 200);

    return (
        <p>
            useRef time: {time.current}
            {/* <br />
            const time2: {time2} */}
        </p>
    )
}