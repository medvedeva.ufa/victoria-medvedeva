import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { useRef } from 'react';

export const Homepage = (props) => {
    // console.log('update');
    const [list, setList] = useState([]);
    const [isAbleToLoadMore, setIsAbleToLoadMore] = useState(false);
    const curPage = useRef(0);

    const getList = async () => {
        try {
            const res = await fetch(`https://swapi.dev/api/people/?page=${curPage.current + 1}`);
            const data = await res.json();

            setIsAbleToLoadMore(!!data.next);

            if (data) {
                curPage.current++;
                setList(list => [...list, ...data.results]);
            }

        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        getList()
    }, [])

    return (
        <ul>
            {list.map(({ url, name }) => <li key={url}>{name}</li>)}
            {isAbleToLoadMore && <button onClick={getList}>Load more</button>}
        </ul>
    )
}