$('.slider-text').slick({
    arrows: true
});

$('.slider-main').slick({
    asNavFor: '.slider-min',
    arrows: false
});

// $('.slider-main').slick({
//     asNavFor: '.slider-min',
//     arrows: false
// });


$('.slider-min').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-main',
    arrows: true,
    centerMode: true,
    centerPadding: '0',
    focusOnSelect: true
});