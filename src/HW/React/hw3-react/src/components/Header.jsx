import React from 'react';
import { Link } from 'react-router-dom'
import styled from 'styled-components';
import { ROUTES } from '../navigation/routes';

export const Header = () => {
    return (
        <Container>
            <Link to={ROUTES.MAIN}>Home</Link>
            <Link to={ROUTES.CART}>Cart</Link>
            <Link to={ROUTES.FAVORITES}>Favorites</Link>
        </Container>
    )
}

const Container = styled.header`
    padding: 10px;
    border-bottom: 1px solid black;
    text-align: center;

    a {
        color: black;
        text-decoration: none;
        margin: 0 15px
    }
`