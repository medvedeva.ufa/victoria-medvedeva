import React, {useState} from "react";
import {Card} from "./Card";
import {favoritesStorageService} from "../services/favoritesStorageService";

import {cartStorageService} from "../services/cartStorageService";


export const CardsList = ({data, filterData = () => true}) => {
    const [favorites, setFavorites] = useState(favoritesStorageService.getValue());
    const [inCart, setInCart] = useState(cartStorageService.getValue());

    const handleOnFavoritesClick = (id ) => {
        if (isInFavorite(id)) {
            setFavorites(favoritesStorageService.remove(id));
        } else {
            setFavorites(favoritesStorageService.add(id));
        }
    };

    const handleInCartClick = (id) => {
        if (isInCart(id)) {
            setInCart(cartStorageService.remove(id))
        } else {
            setInCart(cartStorageService.add(id))
        }
    };

    const isInFavorite = (id) => favorites.includes(id);
    const isInCart = (id) => inCart.includes(id);

    return (
        <div className={'container'}>
            <ul className={"cards-list"}>
                {
                    data.filter((e) => filterData(e, favorites, inCart)).map(item =>
                        <Card
                            id={item.id}
                            price={item.price}
                            name={item.name}
                            brand={item.brand}
                            photo={item.photo}
                            color={item.color}
                            article={item.article}
                            key={item.id}
                            handleInCartClick={() => handleInCartClick(item.id)}
                            handleOnFavoritesClick={() => handleOnFavoritesClick(item.id)}
                            isFavorite={isInFavorite(item.id)}
                            isInCart={isInCart(item.id)}
                        />
                    )
                }
            </ul>
        </div>
    )
};