import React from "react";
import './../scss/style.scss'
import PropTypes from 'prop-types';
import styled from "styled-components";



export const Modal = ({
                          header,
                          text,
                          isShown,
                          onCloseClick,
                          onOkClick,
                          actionButtonText,
                          actionButtonText2
                      }) => {
    return (
        <>
            {isShown && (
                <ModalOverlay onClick={(e) => (e.target === e.currentTarget ? onCloseClick() : null)}>
                    <ModalWindow>
                        <div className={'modal-header-wrapper'}>
                            <span>{header}</span>
                            <span onClick={onCloseClick} style={{cursor: "pointer"}}>&times;</span>
                        </div>
                        <div className={'modal-text-wrapper'}>
                            <span className={'modal-text'}>{text}</span>
                        </div>
                        <div className={'modal-action-wrap'}>
                            <button className={"modal-action"} onClick={onOkClick}>{actionButtonText}</button>
                            <button className={"modal-action"} onClick={onCloseClick}>{actionButtonText2}</button>
                        </div>
                    </ModalWindow>
                </ModalOverlay>
            )}
        </>
    )
};
const ModalWindow = styled.div`
 width: 600px;
  background-color: #b3d4fc;
  border-radius: 10px ;
  padding: 15px;
`;

const ModalOverlay = styled.div`
position: fixed;
  top: 0;
  left: 0;
  z-index: 10;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgba(0, 0, 0, .5);
  display: flex;
  align-items: center;
  justify-content: center;
`;

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    actionButtonText: PropTypes.string,
    actionButtonText2: PropTypes.string,
};

Modal.defaultProps = {
    header: 'Header of modal window',
    text: 'Are you wanna do it?',
    actionButtonText: 'Ok',
    actionButtonText2: 'Cancel'
};
