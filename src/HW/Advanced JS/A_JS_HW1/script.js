//Ex.1
const [kiev, berlin, dubai, moscow, paris] = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];
console.log(kiev);
console.log(berlin);
console.log(dubai);
console.log(moscow);
console.log(paris);

//Ex.2
const employee = {
    name: 'Ekaterina',
    salary: 300
};
const {name, salary} = employee;
console.log(name, salary);

//Ex.3
const [value, showValue] = ['value', 'showValue'];
console.log(value);
console.log(showValue);

