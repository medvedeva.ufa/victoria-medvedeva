import {
    createStore,
    combineReducers
} from 'redux';

import { reducer as counterReducer } from './counter';
import { reducer as planetsReducer } from './planets';

const rootReducer = combineReducers({
    counter: counterReducer,
    planets: planetsReducer
})

// Create our store
const store = createStore(rootReducer);

console.log(store.getState());
// store.subscribe(() => console.log('subscribe', store.getState()));

export default store;