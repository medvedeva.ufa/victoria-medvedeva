const btn = document.querySelector('.btn');

const block = document. querySelector('.news');


btn.addEventListener("click", onChangeThemeClick);

function setThemeDark() {
    block.classList.add('dark');
    localStorage.setItem('activeClass', 'dark');
}

function setThemeWhite() {
    block.classList.remove('dark');
    localStorage.removeItem('activeClass');
}

function onChangeThemeClick() {
    if (block.classList.contains('dark')) {
        setThemeWhite();
    } else {
        setThemeDark();
    }
}

function setThemeFromLocalStorage() {
    if(localStorage.getItem('activeClass')) {
        setThemeDark();
    }
}

setThemeFromLocalStorage();


