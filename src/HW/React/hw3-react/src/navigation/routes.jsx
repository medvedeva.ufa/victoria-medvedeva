export const ROUTES = Object.freeze({
    MAIN: '/',
    CART: '/cart',
    FAVORITES: '/favorites'
})