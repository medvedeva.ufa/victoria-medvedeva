import React, {useState} from "react";
import PropTypes from 'prop-types';
import {ModalContainer} from "./ModalContainer";
import {Star} from "./Star";
import {favoritesStorageService} from "../services/favoritesStorageService";
import {cartStorageService} from "../services/cartStorageService";


export const Card = ({id, price, brand, name, photo, color, article, key}) => {

    const [isFavorite, setIsFavorite] = useState(favoritesStorageService.contains(id));
    const [isInCart, setIsInCart] = useState(cartStorageService.contains(id));

    const onFavoritesClick = () => {
        if (isFavorite) {
            favoritesStorageService.remove(id);
        } else {
            favoritesStorageService.add(id);
        }
        setIsFavorite(!isFavorite);
    };

    const toggleItemInCart = () => {
        if (isInCart) {
            cartStorageService.remove(id);
        } else {
            cartStorageService.add(id);
        }
        setIsInCart(!isInCart);
    };


    return (
        <li className={'item'} id={id} key={key}>
            <div className={'item__img'}>
                <img src={photo} alt={'item-img'}/>
            </div>
            <div className={'item__info'}>
                <div className={'item__info-name'}>
                    <span>{name}</span>
                    <span>{brand}</span>
                </div>
                <div className={'item__info-about'}>
                    <span className={'item__info-about-price'}>Price: ${price}</span>
                    <span className={'item__info-about-color'}>Color: {color}</span>
                    <span className={'item__info-about-article'}>Article: {article}</span>
                </div>
                <div className={'item__info-footer'}>
                    <Star className={isFavorite ? "fas fa-star" : "far fa-star"} onClick={onFavoritesClick}/>
                    <ModalContainer
                        id={id}
                        toggleItemInCart={toggleItemInCart}
                        isInCart={isInCart}
                    />
                </div>
            </div>
        </li>
    )
};
Card.propTypes = {
    id: PropTypes.string,
    price: PropTypes.string,
    brand: PropTypes.string,
    name: PropTypes.string,
    photo: PropTypes.string,
    color: PropTypes.string,
    article: PropTypes.string
};

Card.defaultProps = {
    id: '',
    price: '10000',
    brand: 'No name brand',
    name: 'No name model',
    photo: 'https://i.pinimg.com/originals/8a/eb/d8/8aebd875fbddd22bf3971c3a7159bdc7.png',
    color: 'No color',
    article: '1234567-1'
}
