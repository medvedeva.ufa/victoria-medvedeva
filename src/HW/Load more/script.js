$(function () {
    $(".portfolio__item").slice(0, 12).show();
    $(".portfolio__btn").on('click', function (e) {
        e.preventDefault();
        $(".portfolio__item:hidden").slice(0, 12).slideDown();
        if ($(".portfolio__item:hidden").length == 0) {
            $(".portfolio__btn").fadeOut('slow');
        }
        // $('html,body').animate({
        //     scrollTop: $(this).offset().top
        // }, 1500);
    });
});