import React from "react";
import './../scss/style.scss'



export const Modal = ({header, text, style, closeFunction, actionButtonText, actionButtonText2}) => {
    return (
        <div className={'overlay'} onClick={closeFunction}>
            <div style={style}>
                <div className={'modal-header-wrapper'}>
                    <span>{header}</span>
                    <span onClick={closeFunction} style={{cursor: "pointer"}}>&times;</span>
                </div>
                <div className={'modal-text-wrapper'}>
                    <span className={'modal-text'}>{text}</span>
                </div>
                <div className={'modal-action-wrap'}>
                    <button className={"modal-action"} onClick={closeFunction}>{actionButtonText}</button>
                    <button className={"modal-action"} onClick={closeFunction}>{actionButtonText2}</button>
                </div>
            </div>
        </div>

    )
};

export const modalBlue = {
    width: 600,
    backgroundColor: '#b3d4fc',
    borderRadius: 10,
    padding: 15,
};

export const modalRed = {
    width: 600,
    backgroundColor: 'red',
    borderRadius: 10,
    padding: 15,
};