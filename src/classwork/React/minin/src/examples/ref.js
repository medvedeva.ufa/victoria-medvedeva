import React, {useState, useEffect, useRef} from 'react';

import './App.css';

//let renderCount = 1;
function App() {
    //const [renderCount, setRenderCount] = useState(1);
    const [value, setValue] = useState('initial');
    const renderCount = useRef(1);
    const prev = useRef('')

    useEffect(() => {
        renderCount.current++
    });

    useEffect(() => {
        prev.current = value;
    }, [value]);



    return (
        <div>
            <h1>Quantity of renders: {renderCount.current}</h1>
            <h1>Prev value: {prev.current}</h1>
            <input type="text" onChange={(e) => {setValue(e.target.value)}} value={value}/>

        </div>
    )

}

export default App;
