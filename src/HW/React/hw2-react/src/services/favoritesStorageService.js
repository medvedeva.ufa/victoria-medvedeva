import {FAVORITES_STORAGE_KEY} from "./constants";

export const favoritesStorageService = {
    contains: function (id) {
        return this.getValue().includes(id);
    },
    getValue:  function () {
        return JSON.parse(localStorage.getItem(FAVORITES_STORAGE_KEY)) || [];
    },
    add: function (id) {
        localStorage.setItem(FAVORITES_STORAGE_KEY, JSON.stringify([...this.getValue(), id]));
    },
    remove: function (id) {
        localStorage.setItem(FAVORITES_STORAGE_KEY, JSON.stringify(this.getValue().filter(el => el !== id)));
    },
};
