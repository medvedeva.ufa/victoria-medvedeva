import React from 'react';

import {DataContext} from "../../DataContext";
import {CardsList} from "../../components/CardsList";



export const CartPage = ({}) => {
    const filterOnlyItemInCart = (e, _, inCart) => inCart.includes(e.id);

    return (
        <div>
            <DataContext.Consumer>
                {data =>
                    <CardsList data={data}
                               filterData={filterOnlyItemInCart}
                    />
                }
            </DataContext.Consumer>
        </div>
    );
}