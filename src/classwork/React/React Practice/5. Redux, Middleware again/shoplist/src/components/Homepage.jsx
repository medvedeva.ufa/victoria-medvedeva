import React from 'react';
import styled from 'styled-components';
import { useEffect } from 'react';
import { connect } from 'react-redux';

import { Form } from './Form';
import { List } from './List';
import { getList } from '../store/shopList';



export const Homepage = connect(null, { getList })(({ getList }) => {
    useEffect(() => {
        getList()
    }, [])
    return (
        <Container>
            <Heading>Shop List</Heading>
            <Form />
            <List />
        </Container>
    )
})


const Container = styled.div`
    max-width: 500px;
    margin: 40px auto;
`

const Heading = styled.h1`
    margin: 0;
    text-align: center;
`