import React from 'react'
import styled from "styled-components";


export const Item = ({id, price, src, name, color, cart}) => {
    const style = {
        backgroundImage: ("images/plus-icon.png"),
    };

    return (
            <ItemWrapper  key={id}>
                <ImageWrapper>
                    <ImageInItem src={src}/>
                </ImageWrapper>
                <ItenInfoWrapper>
                    <ItemPrice>Price: ${price}</ItemPrice>
                    <ItemPrice>Cart: {cart}</ItemPrice>
                    <ItemName>{name}</ItemName>
                    <ItemColor>Color: {color}</ItemColor>
                    <div>
                        <ItemButton  style={style}>{'Test'}</ItemButton>
                    </div>
                </ItenInfoWrapper>
            </ItemWrapper>
    )

};



const ItemWrapper = styled.li`
width: 100%;
border-radius: 10px;
box-shadow: 0 20px 30px 0 rgba(36, 23, 228, .1);
`;

const ImageWrapper = styled.div`
width: 100%;
height: 200px;
display: flex;
overflow: hidden;
align-items: center;
justify-content: center;
`;
const ImageInItem = styled.img`
width: 100%;
height: auto;
border-radius: 10px 10px 0 0;
`;

const ItenInfoWrapper = styled.div`
display: flex;
flex-direction: column;
padding: 15px;

`;
const ItemPrice = styled.span`
font-size: 18px;
font-weight: bold;
line-height: 1.5;
color: #313158;
`;
const ItemName = styled.span`
font-size: 16px;
font-weight: 500;
line-height: 1.5;
color: #313158;
`;

const ItemColor = styled.span`
font-size: 12px;
line-height: 2;
color: #313158;
`;

const ItemButton = styled.button`
 padding: 5px 10px 5px 20px;
  color: black;
  border: 1px solid transparent;
  border-radius: 10px;
  cursor: pointer;
 
  background-repeat: no-repeat;
  background-size: 19px;
  background-position: left;
  margin-top: 10px;
`;