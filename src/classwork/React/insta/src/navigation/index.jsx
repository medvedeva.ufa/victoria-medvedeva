import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';

import { Create } from '../pages/Create';
import { Homepage } from '../pages/Homepage';
import { ROUTES } from './routes';



export const Navigation = () => (
    <Router>
        <Switch>
            <Route path={ROUTES.CREATE}>
                <Create />
            </Route>
            <Route path={ROUTES.HOMEPAGE}>
                <Homepage />
            </Route>
        </Switch>
    </Router>
)