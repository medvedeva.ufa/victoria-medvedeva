import React from "react";

export const StartScreen = ({startGame}) => {
    return(
        <div>
            <h1>Start Screen</h1>
            <button onClick={startGame}>Start game</button>
        </div>
    )
};