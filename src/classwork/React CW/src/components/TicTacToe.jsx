import React, { useState } from 'react';

// 1.Создаем верстку
// 2.Структура данных сетки (массив)
// 3.Связать данные и верстку
// 4.Логика смены данных

export const TicTacToe = () => {
    // Создаем массив на 9 элементов и заполняем их null
    const [grid, setGrid] = useState(Array.from(new Array(9), () => null))
    // Создаем состояние слежение за очередностью хода
    const [nextState, setNextState] = useState('X');

    // Обрабатываем клик на ячейке
    const makeMove = (cellIndex) => {
        // Проверяем является ли ячейка незанятой
        if (grid[cellIndex] === null) {
            // Обновляем значение ячейки по индексу
            setGrid(prevGrid => prevGrid.map((cell, index) => {
                if (index === cellIndex) {
                    return nextState
                }
                return cell;
            }));
            // Меняем символ следующего хода
            setNextState(prev => prev === "X" ? 'O' : 'X');
        }
    }

    return (
        <div className="container">
            <div className='grid'>
                {/* Строим сетку по данным в списке */}
                {grid.map((cell, i) =>
                    (<div key={i} className='cell' onClick={() => makeMove(i)}>
                        <div className="cell-wrapper">
                            <span className="cell-symbol">{cell}</span>
                        </div>
                    </div>)
                )}

            </div>
        </div>
    )
}