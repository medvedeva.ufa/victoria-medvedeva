// Reducer
const initialState = {
    value: 0
}

export function reducer(state = initialState, action) {
    // Counter
    switch (action.type) {
        case 'INCREMENT':
            return {
                ...state,
                counter: state.counter + 1
            }
        case 'DECREMENT':
            return {
                ...state,
                counter: state.counter - 1
            }
        case 'RESET':
            return {
                ...state,
                counter: 0
            }
        // Default
        default:
            return state
    }
};