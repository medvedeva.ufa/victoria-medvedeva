import {FAVORITES_STORAGE_KEY} from "./constants";

export const favoritesStorageService = {
    contains: function (id) {
        return this.getValue().includes(id);
    },
    getValue:  function () {
        return JSON.parse(localStorage.getItem(FAVORITES_STORAGE_KEY)) || [];
    },
    add: function (id) {
        const newValue = [...this.getValue(), id];
        localStorage.setItem(FAVORITES_STORAGE_KEY, JSON.stringify(newValue));
        return newValue;
    },
    remove: function (id) {
        const newValue = this.getValue().filter(el => el !== id);
        localStorage.setItem(FAVORITES_STORAGE_KEY, JSON.stringify(newValue));
        return newValue;
    },
};
