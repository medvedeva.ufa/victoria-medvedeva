const gulp = require('gulp');

function hello() {
    console.log('hello bro!');
}

exports.hello = hello;


const test1 = (done) => {
    // throw new Error('error in test');

    let count = 3;
    const counter = setInterval(() => {
        console.log(`test 1: ${count}`);
        if (count === 0) {
            clearInterval(counter);
            console.log('done');
            done();
        } else count--;
    }, 1000);
};

const test2 = (done) => {
    let count = 3;

    const counter = setInterval(() => {
        console.log(`test 2: ${count}`);
        if (count === 0) {
            clearInterval(counter);
            console.log('done');
            done();
        } else count--;
    }, 1000);
};

exports.test1 = test1;
exports.test2 = test2;

/* "составные" таски - подзадачи будут выполняться параллельно (одновременно) или последовательно */
exports.ser = gulp.series(test1, test2);
exports.par = gulp.parallel(test1, test2);


function move() {
    return gulp.src('./src/scripts/*.*')
        .pipe(gulp.dest('./dist/js'));
}

function moveImg() {
    return gulp.src('./src/scripts/*.*')
        .pipe(gulp.dest('./dist/js'));
}
exports.moveImg = moveImg;
exports.move = move;