import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const Header = () => {
    return (
        <Container>
            <Link to="/">Homepage</Link>
            <Link to="/create">Create</Link>
        </Container>
    )
}

const Container = styled.header`
    padding: 20px;
    border-bottom: 1px solid black;

    a {
        text-decoration: none;
        color: black;
        display: inline-block;
        margin: 0 20px;
    }
`