import React from 'react'
import {render} from 'react-dom'


function Article() {
    const body = <section>body</section>
    return (
        <div className={hello} style={{color: 'red'}}>
            <h3>title</h3>
            {body}
            <h3>
                creation date: {(new Date()).toDateString()}
            </h3>
        </div>
    )
}

function App() {
    return (
        <div>
            <h1>Title</h1>
            <Article/>
        </div>
    )
}

render(<App/>, document.get)