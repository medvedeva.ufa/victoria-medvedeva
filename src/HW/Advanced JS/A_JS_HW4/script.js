const div = document.querySelector('.wrapper');
const button = document.querySelector('.btn');


function find() {
    fetch('https://api.ipify.org/?format=json')
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            fetch(`http://ip-api.com/json/#${data.ip}`)
                .then((result) => {
                    return result.json();
                })
                .then(data => {
                    console.log(data);
                    addDataToPage(data);
                })
        })
}

function addDataToPage(data) {
    const title = document.createElement('span');
    title.innerText = "Я все о тебе знаю!!";
    div.prepend(title);
    console.log(title);

    const ul = document.createElement('ul');
    ul.innerHTML = `<li>Ты на континенте: ${data.timezone}</li>
                        <li>Ты в стране: ${data.country}</li>
                        <li>Твой регион: ${data.region}</li>
                        <li>Ты сейчас в городе: ${data.city}</li>
                        <li>Твой район: ${data.zip}</li>`;
    div.append(ul);
}

button.addEventListener('click', find);


