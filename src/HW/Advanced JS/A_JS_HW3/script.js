class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(newName) {
        this._name = newName;
    }

    set age(newAge) {
        this._age = newAge;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }
}


const worker1 = new Employee('Nick', 28, 8000);
const frontendDev1 = new Programmer('Max', 23, 5000, 'javascript');
const frontendDev2 = new Programmer('Alex', 35, 2000, 'javascript');
const frontendDev3 = new Programmer('Andrey', 30, 1000, 'javascript');


console.log(worker1);
console.log(frontendDev1);
console.log(frontendDev2);
console.log(frontendDev3);
