import React from 'react';
import styled from 'styled-components';
import {useState} from 'react';
import {connect} from "react-redux";
import {SET_FILTER_VALUE} from "../store/shopList";



const Search = (props) => {
    const [value, setValue] = useState('');

    const onValueChange = (newValue) => {
        props.onFindChange(newValue);
        setValue(newValue);
    };

    // const onSubmit = (e) => {
    //     e.preventDefault();
    //
    //     if (value.trim() !== '') {
    //         createItem(value);
    //         setValue('');
    //     }
    // }


    return (
        <>
            <Heading>Search</Heading>
            <form>
                <Field name="item" value={value} onChange={e => onValueChange(e.target.value)}/>
            </form>
        </>
    )
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => {
    return {
        onFindChange: (value) => {
            dispatch({type: SET_FILTER_VALUE, payload: value});
        }
    }
};


export default connect(
    mapStateToProps, mapDispatchToProps
)(Search);

const Heading = styled.span`
    font-size:20px;
    padding-left:10px;
    
    `

const Field = styled.input`
    background: #f2f2f2;
    border: none;
    border-radius: 40px;
    padding: 15px;
    display: block;
    width: 300px;
    box-sizing: border-box;
    margin-top: 10px;
`