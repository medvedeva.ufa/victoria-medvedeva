// ACTION TYPES
const SET_NOTES = 'SET_NOTES';
const ADD_NOTE = 'ADD_NOTE';

// SELECTORS
export const MODULE_NAME = 'notes';
export const selectNotesList = state => state[MODULE_NAME].list;

// REDUCER
const initialState = {
    list: []
}

export function notesReducer(state = initialState, { type, payload }) {
    switch (type) {
        case SET_NOTES:
            return {
                ...state,
                list: payload,
            }
        case ADD_NOTE:
            return {
                ...state,
                list: [...state.list, payload]
            }
        default:
            return state
    }
}

// ACTION CREATORS
export const setNotes = payload => ({
    type: SET_NOTES,
    payload
})

export const addNote = payload => ({
    type: ADD_NOTE,
    payload
});


// MIDDLEWARE

export const getNotes = () => async (dispatch) => {
    try {
        const res = await fetch('http://localhost:3005/notes');
        const data = await res.json();

        dispatch(setNotes(data));
    } catch (err) {
        dispatch({
            type: 'SHOW_ERROR',
            payload: err.message
        })
    }
}

export const createNote = note => async (dispatch) => {
    try {
        const fullNote = {
            id: `${Date.now()}${Math.random()}`,
            ...note
        };
        const res = await fetch('http://localhost:3005/notes', {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json'
            },
            body: JSON.stringify(fullNote)
        });

        const data = await res.json();

        dispatch(addNote(fullNote));
    } catch (err) {
        dispatch({
            type: 'SHOW_ERROR',
            payload: err.message
        })
    }
}
