const code = require("../factorial");

describe("factorial", () => {
    test("0 -> 1", () => {
        expect(code.factorial(0)).toBe(1);
    });
    test("1", () => {
        expect(code.factorial(1)).toBe(1);
    });
    test("2", () => {
        expect(code.factorial(2)).toBe(2);
    });
    test("5", () => {
        expect(code.factorial(5)).toBe(120);
    });
    test("-5", () => {
        expect(code.factorial(-5)).toBe(undefined);
    });
    describe("wrong types", () => {
        test("symbol", () => {
            expect(code.factorial('a')).toBe(undefined);
        });
        test("string", () => {
            expect(code.factorial('string')).toBe(undefined);
        });
        test("boolean", () => {
            expect(code.factorial(true)).toBe(undefined);
        });
        test("object", () => {
            expect(code.factorial({})).toBe(undefined);
        })
    })
});
