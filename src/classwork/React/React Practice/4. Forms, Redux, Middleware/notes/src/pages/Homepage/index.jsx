import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import { selectNotesList, getNotes } from '../../store/notes';
import styled from 'styled-components';

const mapStateToProps = state => ({
    list: selectNotesList(state)
})

export const Homepage = connect(mapStateToProps, { getNotes })(
    ({ list, getNotes }) => {


        useEffect(() => {
            getNotes();
        }, [])

        return (
            <Container>
                {list.map(note => (
                    <div key={note.id}>{note.title}</div>
                ))}
            </Container>
        )
    })

const Container = styled.div`
    max-width: 1000px;
    margin: 45px auto;
    display: flex;
    flex-wrap: wrap;
`