const div = document.querySelector('.wrapper');
const button = document.querySelector('.btn');

function addDataToPage(data) {
    const title = document.createElement('span');
    title.innerText = "Я все о тебе знаю!!";
    div.prepend(title);
    console.log(title);

    const ul = document.createElement('ul');
    ul.innerHTML = `<li>Ты на континенте: ${data.timezone}</li>
                        <li>Ты в стране: ${data.country}</li>
                        <li>Твой регион: ${data.region}</li>
                        <li>Ты сейчас в городе: ${data.city}</li>
                        <li>Твой район: ${data.zip}</li>`;
    div.append(ul);
}
async function newSearch() {
    const getIpData = await fetch('https://api.ipify.org/?format=json');
    const ip = await getIpData.json();
    console.log(ip.ip);
    const getAdressData = await fetch(`http://ip-api.com/json/#${ip.ip}`);
    const adressData = await getAdressData.json();
    console.log(adressData);
    addDataToPage(adressData);
}

button.addEventListener('click', newSearch);


