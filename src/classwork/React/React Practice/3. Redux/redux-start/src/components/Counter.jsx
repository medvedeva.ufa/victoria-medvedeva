import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
    value: state.counter
});

// const preparedConnect = connect(mapStateToProps);

export const Counter = connect(mapStateToProps)((props) => {
    console.log('Counter update', props);

    return (
        <div>
            <h1>Counter: {props.value}</h1>
        </div>
    )
})

// export const ConnectedCounter = preparedConnect(Counter);