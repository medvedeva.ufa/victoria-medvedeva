import {CART_STORAGE_KEY, FAVORITES_STORAGE_KEY} from "./constants";

export const cartStorageService = {
    getValue:  function () {
        return JSON.parse(localStorage.getItem(CART_STORAGE_KEY)) || [];
    },
    add: function (id) {
        localStorage.setItem(CART_STORAGE_KEY, JSON.stringify([...this.getValue(), id]));
    },
    contains: function (id) {
        return this.getValue().includes(id);
    },
    remove: function (id) {
        localStorage.setItem(CART_STORAGE_KEY, JSON.stringify(this.getValue().filter(el => el !== id)));
    },
};
