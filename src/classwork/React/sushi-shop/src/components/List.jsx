import React from 'react'
import styled from "styled-components";
import {Item} from './Item'
import {connect, useDispatch, useSelector} from "react-redux";
import {selectShopList, removeItem, SET_FILTER_VALUE, MODULE_NAME} from "../store/shopList";


const List = (props) => {


    console.log(props.list);


    return (
        <ListWrapper>
            {props.list.map(({id, price, image, name, group, cart}) => (

                <Item price={price} key={id} src={image} name={name} color={group} cart={cart}/>
            ))}

        </ListWrapper>

    )
};





function filterByName(list, name) {
    if (!name)
        return list;

    return list.filter(item => item.name.toLowerCase().includes(name.toLowerCase()));
}

const mapStateToProps = state => ({
    list: filterByName(state[MODULE_NAME].list, state[MODULE_NAME].filterByName),
});


const mapDispatchToProps = dispatch => {
    return {
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(List);

const ListWrapper = styled.ul`
list-style: none;
 display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: 1fr 1fr 1fr 1fr;
  gap: 30px 30px;
  padding: 0
`;
