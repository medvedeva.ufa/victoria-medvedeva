// ACTION TYPES
const SET_LIST = 'SET_LIST';
const ADD_LIST = 'ADD_LIST';

// SELECTORS
export const MODULE_NAME = 'list';
export const selectNotesList = state => state[MODULE_NAME].list;

// REDUCER
const initialState = {
    list: []
};

export function notesReducer(state = initialState, { type, payload }) {
    switch (type) {
        case SET_LIST:
            return {
                ...state,
                list: payload,
            };
        case ADD_LIST:
            return {
                ...state,
                list: [...state.list, payload]
            };
        default:
            return state
    }
}

// ACTION CREATORS
export const setList = payload => ({
    type: SET_LIST,
    payload
});

export const addList = payload => ({
    type: ADD_LIST,
    payload
});


// MIDDLEWARE

export const getList = () => async (dispatch) => {
    try {
        const res = await fetch('http://localhost:3000/list');
        const data = await res.json();

        dispatch(setList(data));
    } catch (err) {
        dispatch({
            type: 'SHOW_ERROR',
            payload: err.message
        })
    }
}

export const createList = note => async (dispatch) => {
    try {
        const fullList = {
            id: `${Date.now()}${Math.random()}`,
            ...note
        };
        const res = await fetch('http://localhost:3000/list', {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json'
            },
            body: JSON.stringify(fullList)
        });

        const data = await res.json();

        dispatch(addList(fullList));
    } catch (err) {
        dispatch({
            type: 'SHOW_ERROR',
            payload: err.message
        })
    }
}
