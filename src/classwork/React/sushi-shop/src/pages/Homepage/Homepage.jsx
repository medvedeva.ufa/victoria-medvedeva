import React, {useEffect} from "react";
import {connect} from "react-redux";
import styled from "styled-components";

import {getList} from '../../store/shopList';
import {Pagination} from "../../components/Pagination";
import Search from "../../components/Search";
import List from "../../components/List";
import {Header} from "../../components/Header";

export const Homepage = connect(null, {getList})(({getList}) => {
    useEffect(() => {
        getList()
    }, []);
    return (
        <Container>
            <Header/>
            <Search/>
            <List/>
            <Pagination/>
        </Container>
    )
});


const Container = styled.div`
    max-width: 1200px;
    margin: 0 auto;
`;
