import React from 'react';

// 1. Сделать навигацию с главной страницы на страницу отдельной заметки
// 2. Получить и вывести в консоль на странице заметки её id
// 3. Подготовить reducer, action types, action creators, middleware для single
// --- Создание single.js в store/
// --- Создание AT SET_NOTE
// --- Создание Reducer
// --- Создание AC setNote
// --- Создание middleware getNote
// === Подключить reducer single к store
// 4. Приконектить данные из single к компоненту SingleNote
// 5. Запуск получения данных заметки при монтировании
// 6. Отображение данных заметки на странице

export const SingleNote = () => {
    return (<h1>SingleNote</h1>)
}