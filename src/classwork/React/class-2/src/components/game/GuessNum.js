import React,  {useRef, useState} from "react";
import {StartScreen} from "./StartScreen";

import {GameScreen} from "./GameScreen";
import {FinishScreen} from "./FinishScreen";

export const GuessGame = () => {
    const [stage, setStage] =useState(1);

    const startGame = () => setStage(2);
    const finishGame = () => setStage(3);

    return(
        <div>
            {stage === 1 && <StartScreen  startGame={startGame}/>}
            { stage === 2 && <GameScreen finishGame={finishGame}/>}
            {stage === 3 && <FinishScreen startGame={startGame}/>}
        </div>
    )
};

