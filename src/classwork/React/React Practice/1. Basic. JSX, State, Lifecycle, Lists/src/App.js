import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { TicTacToe } from './components/TicTacToe';


function App() {
  const [state, setState] = useState([1, 2, 3, 4, 5, 6]);
  const liList = state.map(num => <li key={num}>{num}</li>);

  const appendToList = () => setState(state => [...state, Math.random()])

  return (
    <div className="App">
      {/* <ul>
        {liList}
        <button onClick={appendToList}>Add</button>
      </ul> */}
      <TicTacToe />
    </div>
  );
}

export default App;
