import React, {useState, useEffect, useRef, useMemo} from 'react';

import './App.css';

function complexComrute(num) {
    let i = 1;
    while (i < 1000000) i++;
    return num * 2;
}
function App() {
    const [number, setNumber] = useState(42);
    const [colored, setColored] = useState(false);
    const styles = {
        color: colored ? 'blue' : 'red'
    };

    const comruted = useMemo(() => {
        return complexComrute(number);
    }, [number])

    return (
        <div>
            <h1 style={styles}>Number: {comruted}</h1>
            <button onClick={() => setNumber(prevNumber => prevNumber + 1)}>Add</button>
            <button onClick={() => setNumber(prevNumber => prevNumber - 1)}>Remove</button>
            <button onClick={() => setColored(prevNumber => !prevNumber)}>Change</button>


        </div>
    )

}

export default App;
