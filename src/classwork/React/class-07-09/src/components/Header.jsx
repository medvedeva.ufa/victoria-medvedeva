import React from 'react';
import { Link } from 'react-router-dom'
import styled from 'styled-components';
import { ROUTES } from '../navigation/routes';

export const Header = () => {
    return (
        <Container>
            <Link to={ROUTES.HOMEPAGE}>Feed</Link>
            <Link to={ROUTES.CREATE}>Create post</Link>
        </Container>
    )
}

const Container = styled.header`
    padding: 10px;
    border-bottom: 1px solid black;
    text-align: center;

    a {
        color: black;
        text-decoration: none;
        margin: 0 15px
    }
`