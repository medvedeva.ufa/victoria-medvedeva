import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import { Homepage, Create, SingleNote } from './pages';
import { Header } from './commons/Header';

import store from './store'


function App() {
  return (
    <Provider store={store}>
      <Router>
        <Header />
        <Switch>
          <Route path="/create">
            <Create />
          </Route>
          <Route path="/note/:id">
            <SingleNote />
          </Route>
          <Route path="/">
            <Homepage />
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
