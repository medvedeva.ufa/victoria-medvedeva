// Action types
const ADD_PLANETS_AND_UPDATE_NEXT = "ADD_PLANETS_AND_UPDATE_NEXT";
const SET_PLANETS_TOTAL = "SET_PLANETS_TOTAL";


// Reducer
const initialState = {
    list: [],
    total: null,
    nextUrl: 'https://swapi.dev/api/planets/'
}

export function reducer(state = initialState, { type, payload }) {
    // Counter
    switch (type) {
        // Planets
        case ADD_PLANETS_AND_UPDATE_NEXT:
            return {
                ...state,
                list: [...state.list, ...payload.list],
                nextUrl: payload.url
            }
        case SET_PLANETS_TOTAL:
            return {
                ...state,
                total: payload
            }
        // Default
        default:
            return state
    }
};

// Action creators
export const addPlanetsAndUpdateNext = payload => ({
    type: ADD_PLANETS_AND_UPDATE_NEXT,
    payload
});
export const setPlanetsTotal = payload => ({
    type: SET_PLANETS_TOTAL,
    payload
});
// addPlanets([1]) // {type: 'ADD_PLANETS', payload: [1]}