const code = require("../script");

describe("tests for sum", () => {
    test("0 + 0 = 0", () => {
        // given
        const first = 0;
        const second = 0;

        // when
        const result = code.sum(first, second);

        // then
        expect(result).toBe(0);
    });

    test("0 + 1 = 1", () => {
        // given
        const first = 0;
        const second = 1;

        // when
        const result = code.sum(first, second);

        // then
        expect(result).toBe(1);
    });
});
