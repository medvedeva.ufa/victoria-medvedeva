import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { selectPostsList, getPosts } from '../../store/posts';

import { SinglePost } from './SinglePost';
import { Header } from '../../components/Header';


const mapStateToProps = state => ({
    posts: selectPostsList(state)
})

export const Homepage = connect(mapStateToProps, { getPosts })(
    ({ posts, getPosts }) => {

        useEffect(() => {
            getPosts();
        }, [getPosts]);

        return (
            <div>
                <Header />
                {posts.map(post => (
                    <SinglePost key={post.id} {...post} />
                ))}
            </div>
        );
    })