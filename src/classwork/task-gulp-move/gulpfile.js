const gulp = require('gulp');

function move() {
    return gulp.src('./src/scripts/*.*')
        .pipe(gulp.dest('./dist/js'));
}
function moveImg() {
    return gulp.src('./src/img/**/*.*')
        .pipe(gulp.dest('./dist/img'));
}
exports.move = move;
exports.moveImg = moveImg;

exports.ser = gulp.series(move, moveImg);

