
const tabs = document.querySelectorAll('.tabs-title');
const contentList = document.querySelectorAll('.content');
tabs.forEach(function (item, i) {
    item.addEventListener('click', function () {
        const dataTab = this.dataset.tab;
        tabContent = document.querySelector('.content[data-tab="'+dataTab+'"]');
        document.querySelectorAll('.tabs-title.active').forEach(function (item, i) {
            item.classList.remove('active');
        });
        contentList.forEach(function (item, i) {
            item.classList.remove('active');
        });
        tabContent.classList.add('active');
        this.classList.add('active');
    })
});

