import axios from "axios";


export const SET_ITEMS = 'SET_ITEMS';
export const SET_FILTER_VALUE = 'SET_FILTER_VALUE';
export const ADD_TO_CART = 'ADD_TO_CART';


export const MODULE_NAME = 'shoplist';
export const selectShopList = state => state[MODULE_NAME].list;

const initialState = {
    list: [],
    filterByName: "",
    addToCart: false
};

export function reducer(state = initialState, {type, payload}) {
    switch (type) {
        case SET_ITEMS:
            return {
                ...state,
                list: payload,
            };
        case SET_FILTER_VALUE:
            return {
                ...state,
                filterByName: payload
            };
        default:
            return state;
    }
}

export const setItems = payload => ({
    type: SET_ITEMS,
    payload
});

export const setItemsFull = function (payload) {
    return {
        type: SET_ITEMS,
        payload: payload
    };
};

//
// /* -------------------------------------------------------------------------------- */
// const str = {
//     count: 0,
//     age: 0,
// };
//
//
// function setCount(param) {
//     str.count = param;
// }
//
// function setAge(param) {
//     str.age = param;
// }
//
// setCount(11);
// setAge(100);
//
// ///////////
//
// function magicShit_reducer(prevStore, type, param) {
//     switch (type) {
//         case "age":
//             return {
//                 ...prevStore,
//                 age: param
//             };
//         case "count":
//             return {
//                 ...prevStore,
//                 count: param
//             };
//         case "age_count":
//             return {
//                 ...prevStore,
//                 age: param
//                 count: param
//             };
//         default
//             return prevStore;
//     }
// }
//
// magicShit_reducer(str, "count", 11);
// magicShit_reducer(str, "age", 100);
// magicShit_reducer(str, "age_count", 100);
//
// console.log(str.count);
//
//
// function abc(k) {
//
// }
//
// abc("k");
//
// const b = {
//     type: "abc",
//     payload: "k"
// };


export const getList = () => async dispatch => {
    try {
        const {data} = await axios.get("http://localhost:3000/list");
        dispatch(setItems(data))

    } catch (error) {
        console.log(error);
    }
};

