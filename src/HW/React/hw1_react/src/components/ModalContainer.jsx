import React, {useState} from "react";


import {Button} from "./Button";
import {Modal} from "./Modal";
import {modalBlue} from "./Modal";
import {modalRed} from "./Modal";
import {firstModalContent, secondModalContent} from "../content/ModalContent";



export const ModalContainer = () => {

    const [activeFirst, setActiveFirst] = useState(false);
    const [activeSecond, setActiveSecond] = useState(false);

    const activeFirstModal = () => setActiveFirst(true);
    const activeSecondModal = () => setActiveSecond(!activeSecond);

    const closeModalFirst = () => setActiveFirst(false);
    const closeModalSecond = () => setActiveSecond(false);

    return (
        <>
                <Button showFunction={activeFirstModal} text={'Open first button'} backgroundColor={'red'} className={'button'}/>
            { activeFirst &&
            <Modal header={firstModalContent.header} text={firstModalContent.text} style={modalBlue} closeFunction={closeModalFirst} actionButtonText={'Ok'} actionButtonText2={'Cancel'}/>}

            <Button showFunction={activeSecondModal} text={'Open second button'} backgroundColor={'blue'} className={'button'}/>
            { activeSecond
            && <Modal header={secondModalContent.header} text={secondModalContent.text} style={modalRed} closeFunction={closeModalSecond} actionButtonText={'Ok'} actionButtonText2={'Cancel'}/>}
    </>
)
};