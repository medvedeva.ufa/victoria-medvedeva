import React from "react";
import styled from "styled-components";
import {Form} from "./Form";

export const Homepage = () => {
    return (
        <Container>
            <Heading>Shop list</Heading>
            <Form/>
        </Container>
    )
};

const Container = styled.div`
max-width: 500px;
margin: 40px auto
`;

const Heading = styled.h1`
margin: 0
`;
