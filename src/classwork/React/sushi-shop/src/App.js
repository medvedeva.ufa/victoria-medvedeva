import React from 'react';
import { Provider } from 'react-redux';
import {store} from "./store";


import './App.css';
import {Homepage} from './pages/Homepage/Homepage';
import {Navigation} from "./navigation";


function App() {
    return (
        <Provider store={store}>
            <Navigation/>
        </Provider>

    );
}

export default App;
