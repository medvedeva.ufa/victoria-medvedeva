let poly = 1221;
let ylop = '';
let isPalindrome = false;


let polyCopy = poly;
let polyStr = String(poly);
let currentNumber;
for (var i = 0; i < polyStr.length; i++ ) {
    currentNumber = polyCopy % 10;
    ylop += currentNumber;
    polyCopy = Math.floor(polyCopy / 10);
}

if (ylop == poly) {
    isPalindrome = true;
}