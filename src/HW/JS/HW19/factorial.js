module.exports.factorial = function (n) {
    if (n < 0 || typeof n !== "number") {
        return undefined
    }

    let result = 1;
    for (let i = 2; i <= n; i++ ) {
        result *= i;
    }
    return result;
};