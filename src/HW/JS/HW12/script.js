
const delay = 2000;
const wrap = document.querySelector(".images-wrapper");


let slider = setInterval(showImage, delay);


function showImage() {
    const images = Array.from(wrap.children);
    let index = images.findIndex(img => !img.classList.contains("hide"));
    images[index].classList.toggle('hide');
    index++;
    if (index >= images.length) index = 0;
    images[index].classList.toggle('hide');
}

const pauseButton = document.createElement('button');
const continueButton = document.createElement('button');
pauseButton.textContent = 'Прекратить';
continueButton.textContent = 'Возобновить показ';
wrap.before(pauseButton);
wrap.before(continueButton);


document.addEventListener('click', function onClick(event) {
    if (event.target === pauseButton) {
        clearInterval(slider);

    }
    if (event.target === continueButton) slider = setInterval(showImage, delay);
});
