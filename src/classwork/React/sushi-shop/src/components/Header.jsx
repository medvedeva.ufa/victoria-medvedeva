import React from 'react';
import {Link, BrowserRouter as Router} from 'react-router-dom'
import styled from 'styled-components';
import {ROUTES} from "../navigation/routes";


export const Header = () => {
    return (
        <Container>
                <Link to={ROUTES.HOMEPAGE}>Homepage</Link>
                <Link to={ROUTES.CART}>Your cart</Link>
        </Container>
    )
}

const Container = styled.div`
    padding: 20px 0;
    border-bottom: 1px solid black;
    display: flex;
    justify-content: space-between;

    a {
        color: black;
        text-decoration: none;
        margin: 0 15px
    }
`