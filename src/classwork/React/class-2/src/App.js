import React, {useRef} from 'react';
import logo from './logo.svg';
import './App.css';
import './style.sass'
import './components/UseRefTest'
import {GuessGame} from "./components/game/GuessNum";


function App() {
    return (
        <div className="App">

            <GuessGame/>
        </div>
    );
}

export default App;
