import React from 'react';
import styled from 'styled-components';


export const SinglePost = ({ image, title }) => {
    return (
        <Container>
            <Image alt={title} src={image} />
            <Title>{title}</Title>
        </Container>
    )
}

const Container = styled.div`
    max-width: 600px;
    margin: 30px auto;
`

const Image = styled.img`
    width: 100%;
    height: 400px;
    object-fit: cover;
    object-position: center;
`

const Title = styled.h5`
    margin: 20px 0 0;
`