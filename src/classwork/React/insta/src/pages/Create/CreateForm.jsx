import React from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { createPost } from '../../store/posts'
import { useHistory } from 'react-router-dom'

const validationSchema = Yup.object().shape({
    image: Yup.string()
        .min(5, 'Too Short!')
        .max(250, 'Too Long!')
        .required('Required'),
    title: Yup.string()
        .min(3, 'Too Short!')
        .max(30, 'Too Long!')
        .required('Required'),
})


export const CreateForm = connect(null, { createPost })(({ createPost }) => {

    const history = useHistory();

    const initialValues = {
        image: '',
        title: ''
    };

    const onSubmit = (values) => {
        createPost(values, history)
    }

    return (
        <Container>
            <Formik
                initialValues={initialValues}
                onSubmit={onSubmit}
                validationSchema={validationSchema}
            >
                {({ errors }) => (
                    <Form>
                        <StyledField name="image" />
                        <StyledField name="title" />
                        <Btn type="submit">Create</Btn>

                        {(errors.image || errors.title) &&
                            <ErrorMessage>
                                {errors.image}
                                {(errors.image && errors.title) && <br />}
                                {errors.title}
                            </ErrorMessage>
                        }
                    </Form>)
                }
            </Formik>
        </Container>
    )
})

const Container = styled.div`
    max-width: 500px;
    margin: 40px auto;
`

const ErrorMessage = styled.div`
    padding: 10px;
    background-color: tomato;
    color: white;
    text-align: center;
    border-radius: 5px;
    margin-top: 15px;
`

const StyledField = styled(Field)`
    display: block;
    width: 100%;
    height: 30px;
    margin-top: 15px;
`

const Btn = styled.button`
    display: block;
    width: 100%;
    height: 30px;
    margin-top: 15px;
`