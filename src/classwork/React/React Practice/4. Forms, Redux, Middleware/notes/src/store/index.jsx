import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'

import { notesReducer, MODULE_NAME as notesModuleName } from './notes'

const rootReducer = combineReducers({
    [notesModuleName]: notesReducer
})

const store = createStore(rootReducer, applyMiddleware(thunk));
// console.log(store.getState());

export default store;