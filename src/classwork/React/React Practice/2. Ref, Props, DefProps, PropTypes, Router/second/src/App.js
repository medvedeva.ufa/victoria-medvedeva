import React, { useState } from 'react';
// import logo from './logo.svg';
import './App.css';
import './style.sass';

import { RefTest } from './components/RefTest';
import { GuessGame } from './components/GuessNum';
import { PropsTest } from './components/PropsTest';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink
} from "react-router-dom";
import { Product } from './components/Product';

function App() {

  return (
    <Router>
      <div className="App">
        <header className="header">
          <NavLink to="/" exact>Home</NavLink>
          <NavLink to="/game">Game</NavLink>
        </header>

        <Switch>
          <Route path="/game">
            <GuessGame />
          </Route>

          <Route path="/products/:id">
            <Product />
          </Route>

          <Route path="/">
            <h1>Homepage</h1>
          </Route>

        </Switch>
      </div>
    </Router>
  );
}

export default App;











function CounterIncrement({ setValue }) {

  console.log('Counter updated');
  const onClickHandler = () => setValue(v => v + 1)
  return (
    <h1
      onClick={onClickHandler}
    >
      +
    </h1>
  )
}
function CounterDecrement({ setValue }) {

  const onClickHandler = () => setValue(v => v - 1)
  console.log('Counter updated');
  return (
    <h1
      onClick={onClickHandler}
    >
      -
    </h1>
  )
}

function SomethingElse(props) {
  console.log('SomethingElse updated');
  return (
    <h1>Current value: {props.value}</h1>
  )
}
