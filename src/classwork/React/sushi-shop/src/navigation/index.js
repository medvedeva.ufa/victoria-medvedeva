import React from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {ROUTES} from "./routes";


import {Cart} from "../pages/Cart/Cart";
import {Homepage} from "../pages/Homepage/Homepage";

export const Navigation = () => {
    return (
        <Router>
            <Switch>
                <Route path={ROUTES.CART}>
                    <Cart/>
                </Route>
                <Route path={ROUTES.HOMEPAGE}>
                    <Homepage/>
                </Route>
            </Switch>
        </Router>
    )
};