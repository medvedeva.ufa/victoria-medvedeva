import React from 'react';
import { Header } from '../../components/Header';
import { CreateForm } from './CreateForm';


export const Create = () => {
    return (
        <div>
            <Header />
            <CreateForm />
        </div>
    );
}