/* Условие задачи

- Написать функцию-"фабрику", которая возвращает объекты пользователей.

- Объект пользователя обладает тремя свойствами:
    - Имя;
- Фамилия;
- Профессия.
- методом sayHi, который выводит в консоль сообщение 'Привет, меня зовут <имя> <фамилия>. Я <профессия>'.

    Функция-фабрика принимает три параметра,
    которые отражают вышеописанные свойства объекта.
    Каждый параметр функции обладает значением по-умолчанию: null.
    
   
 */
/*
let user = {
  name: null,
  surname: null,
    profession: null,
    sayHi() {
      alert('Hi! My name is ' + this.name + " " + this.surname + '. I am ' + this.profession)
    }

};

function introduce(userName, userSurname, userProfession) {
user.name = userName;
user.surname = userSurname;
user.profession = userProfession;
    return user.sayHi();
}

introduce('Vasya', 'Ivanov', 'doctor');

*/
/*
# Условие задачи

Пользуясь данными с сайта https://en.wikipedia.org/wiki/List_of_countries_by_population_(United_Nations),
    представьте их в виде объекта с именем countries данные по каждой стране.

    Рассчитайте для каждой страны прирост населения за год и процент, который составляет население этой страны от общемирового населения (приняв его равным 7713468000) и добавьте эти данные в объект.

    Выведите данные в консоль в формате "Страна: общее население ( % от общемирового),

Доп. задание: рассчитайте, какой процент от общемирового населения составляют все эти страны в совокупности, выведите эту информацию в консоль.
*/

const population = 7713468000;

const countries = {
  china: {
    year: {
      2018: 1427647786,
      2019: 1433783686,
    },
  },
  india: {
    year: {
      2018: 1352642280,
      2019: 1366417754,
    },
  },
  usa: {
    year: {
      2018: 327096265,
      2019: 329064917,
    },
  },
  indonesia: {
    year: {
      2018: 267670543,
      2019: 270625568,
    },
  },
  pakistan: {
    year: {
      2018: 212228286,
      2019: 216565318,
    },
  }
};

for (const country in countries) {
  console.log(country);
  let pop2018 = countries[country].year[2018];
  let pop2019 = countries[country].year[2019];
 countries[country].growth = (((pop2019 - pop2018) / pop2018) * 100).toFixed(2) + "%";


}

// function protect(email) {
//   let lastPart = email.indexOf('@');
//   let firstPart = email.slice(0, lastPart);
//   let middleOfEmail = Math.floor(firstPart.length / 2);
//   let qwerty = email.slice(0, middleOfEmail);
//   return email.replace(qwerty, '...');
//
//
//
// }
// console.log(protect("medvedeva.ufa@gmail.com"));

// console.log(protectEmail("mysecretemail@gmail.com"));
//
// function protectEmail(email) {
//   let result = email.split("@");
//   console.log(result[0]);
//   return (
//       result[0].substring(0, result[0].length / 2) + "..." + "@" + result[1]
//   );
// }

/* Условие задачи

Напишите функцию, которая будет принимать строку и возвращать аббревиатуру (из заглавных первых букв слов).

пример работы функции

    ```js
console.log(abbreviate('nine inch nails');  // NIN
```
for (let i = 0; i < 3; i++) {
}

 */
// let abbr = '';
// function abbreviate(string) {
//   let qwerty = string.split(' ');
//   for (let i = 0; i < qwerty.length; i++) {
//
//     abbr += qwerty[i][0].toUpperCase();
//   }
//   return abbr;
//
// }
// console.log(abbreviate('Маша мыла раму шило'));
