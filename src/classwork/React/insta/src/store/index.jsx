import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';

import { MODULE_NAME as postsModuleName, reducer as postsReducer } from './posts';
import { MODULE_NAME as authModuleName, reducer as authReducer } from './auth';

const rootReducer = combineReducers({
    [postsModuleName]: postsReducer
});

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));