import React, {useState} from "react";

import {Button} from "./Button";
import {Modal} from "./Modal";


export const ModalContainer = ({isInCart, toggleItemInCart}) => {

    const [activeModal, setActiveModal] = useState(false);

    const toggleModal = () => {
        setActiveModal(!activeModal)
    };

    const onAddToCartHandler = () => {
        toggleModal();
        toggleItemInCart();
    };

    return (
        <>
            <Button text={isInCart ? "Remove from cart" : 'Add to cart'} onClick={toggleModal} style={
                isInCart ? styles.inCartStyles : styles.notInCartStyles
            }/>
            <Modal
                isShown={activeModal}
                header={'Accept your action'}
                text={'Are you sure?'}
                actionButtonText={'Yes'}
                actionButtonText2={'No'}
                onClose={toggleModal}
                onOkClick={onAddToCartHandler}
                onCloseClick={toggleModal}
            />
        </>
    )
};

const styles = {
    inCartStyles: {
        backgroundColor: "salmon"
    },
    notInCartStyles: {
        backgroundColor: "steelBlue"
    }
};
