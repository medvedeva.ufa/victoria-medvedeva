import React, { useRef, useState } from 'react';

// Загадываем число
// Создаем вестку (Кнопка + Поле ввода + Заголовок сообщения)
// Слушаем  поле на изменение и запоминаем значение
// При нажании на кнопку сравниваем значения чисел:
// Если число совпало - Идем на stage 3
// Если не совпало - Обновляем подсказку

export const GameScreen = ({ finishGame }) => {
    const number = useRef(randomInteger(1, 100));
    const userNum = useRef(0);
    const [message, setMessage] = useState('');

    const updateUserNum = (e) => {
        if (message !== '') setMessage('');

        userNum.current = +e.target.value;
    }

    const tryGuess = () => {
        if (number.current === userNum.current) {
            finishGame()
        } else {
            const hint = number.current > userNum.current ? 'Lower' : 'Higher';
            setMessage(hint);
        }
    };

    return (
        <div>
            {number.current}
            <input type="number" onChange={updateUserNum} />
            <button onClick={tryGuess}>try</button>
            <h4>{message}</h4>
        </div>
    )
};

function randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
}
