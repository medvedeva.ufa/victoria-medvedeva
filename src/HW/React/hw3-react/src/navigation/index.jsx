import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';


import {ROUTES} from './routes';
import {CartPage} from "../pages/Cart/CartPage";
import {FavoritesPage} from "../pages/Favorites/FavoritesPage";
import {MainPage} from "../pages/Main/MainPage";
import {Header} from "../components/Header";


export const Navigation = () => (
    <Router>
        <Header/>
        <Switch>
            <Route path={ROUTES.CART}>
                <CartPage/>
            </Route>
            <Route path={ROUTES.FAVORITES}>
                <FavoritesPage/>
            </Route>
            <Route path={ROUTES.MAIN}>
                <MainPage/>
            </Route>
        </Switch>
    </Router>
)