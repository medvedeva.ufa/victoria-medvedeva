import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';

function computeInitialCalc() {
    return Math.trunc(Math.random() * 20);
}

function App() {
    const [state, setState] = useState(() => {return computeInitialCalc()});

    function increment() {
        setState((prevState) => {return  prevState + 1});
    }

    function decrement() {
        setState((prevState) => {return  prevState - 1});
    }

    return (
        <div>
            <h1>Counter: {state}</h1>
            <button onClick={increment}>Add</button>
            <button onClick={decrement}>Delete</button>
        </div>
    );
}

export default App;
