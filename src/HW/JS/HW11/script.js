
const div = document.querySelector(".btn-wrapper");

document.addEventListener("keydown", highlightKey);

function highlightKey(event) {
    const currentClass = "active";
    const currentKey = event.key;

    for (let btn of div.children) {
        if(btn.classList.contains(currentClass)) {
            btn.classList.toggle(currentClass);
        }
        if (btn.innerText === currentKey) {
             btn.classList.toggle(currentClass);
        }

    }
}

