import React, {useState, useEffect, useRef, useMemo} from 'react';


import './App.css';


function App() {
  const [count, setCount] = useState(1);
  const [colored, setColored] = useState(false);
  const styles = {
    color: colored ? 'blue' : 'red'
  };

const generateItemsFromAPI = () => {
  return new Array(count).fill('').map((_, i) => `Item ${i + 1}`)
};

    return (
        <div>
          <h1 style={styles}>Number: {count}</h1>
          <button onClick={() => setCount(prevNumber => prevNumber + 1)}>Add</button>
          <button onClick={() => setColored(prevColor => !prevColor)}>Change</button>

        </div>
    )

}

export default App;
