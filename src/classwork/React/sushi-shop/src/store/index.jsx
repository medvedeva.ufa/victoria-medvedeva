import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';

import {
    reducer as shopListReducer,
    MODULE_NAME as shopListModuleName,
} from './shopList'

const rootReducer = combineReducers({
    [shopListModuleName]: shopListReducer
});

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));


