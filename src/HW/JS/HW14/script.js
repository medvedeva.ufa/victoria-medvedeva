const $ = window.$;

$(document).ready(function(){

    $(".nav-item").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
        top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });

    const buttonToTop = $('<button class="btn-to-top">To top</button>').appendTo('body');
    buttonToTop.css({
        'display' : 'none',
        'padding' : '6px 12px',
        'background-color' : 'red',
        'color' : 'white',
        'position' : 'fixed',
        'left' : '90%',
        'top' : '90%',
        'border-radius' : '5px',
    });

    $(window).on('scroll', function () {
        if($(window).scrollTop() >= $(window).height()) {
            buttonToTop.css('display', '');
        } else {
            buttonToTop.css('display', 'none');
        }
    });
    
    $(buttonToTop).on('click', function () {
        $('html').animate({scrollTop: 0}, 1500);
    });

    $(document).on('click', '.slide-toggle', function (){
        $('#posts').slideToggle();
    });


});




