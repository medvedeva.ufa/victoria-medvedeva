import Axios from "axios";
import { APP_API_KEY } from "../API";

const SET_AUTH_SUCCESS = 'SET_AUTH_SUCCESS';
const SET_LOG_OUT = 'SET_LOG_OUT';

export const MODULE_NAME = 'auth';
export const selectUserID = state => state[MODULE_NAME].userID;
export const selectUserToken = state => state[MODULE_NAME].token;

const initialState = {
    userID: null,
    token: null
}

export function reducer(state = initialState, { type, payload }) {
    switch (type) {
        case SET_AUTH_SUCCESS:
            return {
                ...state,
                userID: payload.userID,
                token: payload.token
            }
        case SET_LOG_OUT:
            return {
                ...state,
                userID: null,
                token: null
            }
        default:
            return state
    }
}

export const setAuthSuccess = payload => ({
    type: SET_AUTH_SUCCESS,
    payload
});

export const setLogOut = () => ({
    type: SET_LOG_OUT,
});

export const sign = ({ email, password }, isRegistration = false) => async dispatch => {
    try {
        const signType = isRegistration ? 'signUp' : 'signInWithPassword';
        const reqUrl = `https://identitytoolkit.googleapis.com/v1/accounts:${signType}?key=${APP_API_KEY}`;
        const reqData = {
            email,
            password,
            returnSecureToken: true
        }

        const { status, data } = await Axios.post(reqUrl, reqData);

        if (status === 200) {
            dispatch(setAuthSuccess({
                userID: data.localID,
                token: data.idToken
            }));
        }
    } catch (error) {
        console.log(error);
    }
}