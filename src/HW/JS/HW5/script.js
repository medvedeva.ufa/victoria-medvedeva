function createNewUser(firstName, lastname, birthday) {
    let splitDay = birthday.split(".");
    let newBirthDayString = splitDay[1] + '.' + splitDay[0] + '.' + splitDay[2];
    let parsedBirthday = new Date(newBirthDayString);

    return {
        firstName,
        lastname,
        birthday: parsedBirthday,
        getLogin() {
            return  firstName.slice(0, 1).toLowerCase() + lastname.toLowerCase();

        },
        getAge() {
            let now = new Date();

            let newBirthDayMilli = Date.parse(this.birthday);
            const dayMilli = 1000*60*60*24;
            return Math.floor((+now - +newBirthDayMilli) / dayMilli / 365);
        },
        getPassword() {
            return  this.firstName.slice(0, 1).toUpperCase() + this.lastname.toLowerCase() + this.birthday.getFullYear();
        }
    }
}


let newUser = createNewUser("hghhg", 'hghghghg', '31.12.1990');
console.log(newUser.getPassword());
console.log(newUser.getAge());


