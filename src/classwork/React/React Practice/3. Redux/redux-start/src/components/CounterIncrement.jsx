import React from 'react';
import { connect } from 'react-redux';


export const CounterIncrement = connect()((props) => {
    console.log('CounterIncrement update', props);

    const dispatchIncrement = () => {
        props.dispatch({ type: 'INCREMENT' });
    }
    return (
        <div>
            <button onClick={dispatchIncrement}>+</button>
        </div>
    )
})